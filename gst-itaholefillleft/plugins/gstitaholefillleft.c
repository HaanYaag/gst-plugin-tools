/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstitaholefillleft
 *
 * The itaholefillleft element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! itaholefillleft ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstitaholefillleft.h"

GST_DEBUG_CATEGORY_STATIC (gst_itaholefillleft_debug_category);
#define GST_CAT_DEFAULT gst_itaholefillleft_debug_category

/* prototypes */


static void gst_itaholefillleft_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_itaholefillleft_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_itaholefillleft_dispose (GObject * object);
static void gst_itaholefillleft_finalize (GObject * object);

static gboolean gst_itaholefillleft_start (GstBaseTransform * trans);
static gboolean gst_itaholefillleft_stop (GstBaseTransform * trans);
static gboolean gst_itaholefillleft_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_itaholefillleft_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_itaholefillleft_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_itaholefillleft_src_template =
    GST_STATIC_PAD_TEMPLATE("src", GST_PAD_SRC, GST_PAD_ALWAYS,
                            GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{  GRAY16_LE  }")));

static GstStaticPadTemplate gst_itaholefillleft_sink_template =
    GST_STATIC_PAD_TEMPLATE(
        "sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{ GRAY16_LE }")));


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstItaholefillleft, gst_itaholefillleft, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_itaholefillleft_debug_category, "itaholefillleft", 0,
  "debug category for itaholefillleft element"));

static void
gst_itaholefillleft_class_init (GstItaholefillleftClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itaholefillleft_sink_template);
  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itaholefillleft_src_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "ITA_Distance_Filter_Hole_fill_from_left", "ITA_Distance_Filter_Hole_fill_from_left", "ITA_Distance_Filter_Hole_fill_from_left",
      "Han <yhde93@gmail.com>");

  gobject_class->set_property = gst_itaholefillleft_set_property;
  gobject_class->get_property = gst_itaholefillleft_get_property;
  gobject_class->dispose = gst_itaholefillleft_dispose;
  gobject_class->finalize = gst_itaholefillleft_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_itaholefillleft_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_itaholefillleft_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_itaholefillleft_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_itaholefillleft_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_itaholefillleft_transform_frame_ip);

}

static void
gst_itaholefillleft_init (GstItaholefillleft *itaholefillleft)
{
}

void
gst_itaholefillleft_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (object);

  GST_DEBUG_OBJECT (itaholefillleft, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itaholefillleft_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (object);

  GST_DEBUG_OBJECT (itaholefillleft, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itaholefillleft_dispose (GObject * object)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (object);

  GST_DEBUG_OBJECT (itaholefillleft, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_itaholefillleft_parent_class)->dispose (object);
}

void
gst_itaholefillleft_finalize (GObject * object)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (object);

  GST_DEBUG_OBJECT (itaholefillleft, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_itaholefillleft_parent_class)->finalize (object);
}

static gboolean
gst_itaholefillleft_start (GstBaseTransform * trans)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (trans);

  GST_DEBUG_OBJECT (itaholefillleft, "start");

  return TRUE;
}

static gboolean
gst_itaholefillleft_stop (GstBaseTransform * trans)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (trans);

  GST_DEBUG_OBJECT (itaholefillleft, "stop");

  return TRUE;
}

static gboolean
gst_itaholefillleft_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (filter);

  GST_DEBUG_OBJECT (itaholefillleft, "set_info");

  return TRUE;
}

/* transform */
static GstFlowReturn
gst_itaholefillleft_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (filter);
  gint width = GST_VIDEO_FRAME_WIDTH(inframe);
  gint height = GST_VIDEO_FRAME_HEIGHT(inframe);

  guint16 *in = (guint16 *)inframe->data[0];
  guint16 *out = (guint16 *)outframe->data[0];

    for (guint i = width+1; i < (height-1)*width-1; i++) {
      if(i % width != 0 || (i + 1)%width != 0){
        if(in[i] == 0x0000){

          out[i] = in[i -1] ;  
          
        } else{
          out[i] = in[i];
        }
              }
  }
  GST_DEBUG_OBJECT (itaholefillleft, "transform_frame");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_itaholefillleft_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstItaholefillleft *itaholefillleft = GST_ITAHOLEFILLLEFT (filter);

  GST_DEBUG_OBJECT (itaholefillleft, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "itaholefillleft", GST_RANK_NONE,
      GST_TYPE_ITAHOLEFILLLEFT);
}

/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://FIXME.org/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    itaholefillleft,
    "FIXME plugin description",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

