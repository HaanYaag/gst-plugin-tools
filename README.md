This is a Folder for the Hiwi Jobs in ITA, Uni Hannover, Garbsen

The Turor is Lukas Juette. The Hiwi is Han Yang.
# 0. Finished Plugin Description

## 0.0 How to use these plugins

1. in folder (for example) \gst-ita4nachbar run terminal `meson build`,  build is a name of folder, in which the builded file would stay. 

2. in the same directory position as the last step, and run `ninja -C build`, build should be the same folders name as last step. to that we could create the plugin file. The plugin file would be saved in `\gst-ita4nachbar\build\plugin` as a `.so` file with `libgst` as prefix.

3. **or** you can  create the plugin file and **directly install in your computer** with `sudo ninja -C build install`. The position, that the plugin would be installed is saved in the file `meson.build` in this directory. By changing the argument of `plugins_install_dir = '/opt/aivero/rgbd_toolkit_x86_64/lib/gstreamer-1.0'` to change the install directory.

4. to verify the plugin that you created you could use the tool that gstream provided `gst-inspect-1.0`. To veryfy the plugin, that has not installed in your computer, we could use the fullly filename  of the plugin such as `gst-inspect-1.0 ./libgstita4nachbar.so`. To veryfy the plugin, that has already installed in your computer, you can just use the name of plugin without prefix as argument, such as `gst-inspect-1.0 ita4nachbar`. If you could see the detail information of the plugin, that means the build and compilation of plugin is succed.
But **note**:  I don't suggest, that we together compile and install at the first time, because the install directory of plugin is sometimes in the system folder. To delete a file in such system folder would be difficult (you have to use `sudo rf rm`). 

5. example: 

```
# build

\gst-ita4nachbar: meson build

# compile
\gst-ita4nachbar: ninja -C build

# check
\gst-ita4nachbar\build\plugin\: gst-inspect-1.0 ./libgstita4nachbar.so

# install
\gst-ita4nachbar: sudo ninja -C build install

# check again
\gst-ita4nachbar: gst-inspect-1.0 ita4nachbar

```



## 0.1 gst-itadf0counter

this is a counter, that would count the amount of the pixels with 0 distance in each frames.

## 0.2 gst-itadf0shower

this tool would convert the pixels with 0 distance in each frame into a binary and text formation.


## 0.1 gst-ita4nachbar

this is a distance filter to fill the pixels with 0 distance, by using the average of the 4 neighbors.

||||
|:-:|:-:|:-:|
||**i-width**||
|**i-1**|**i**|**i+1**|
||**i+width**||

## 0.2 gst-ita8nachbar

this is a distance filter to fill the pixels with 0 distance, by using the average of the 8 neighbors.

||||
|:-:|:-:|:-:|
|i-width-1|i-width|i-width+1|
|**i-1**|**i**|**i+1**|
|**i+width-1**|**i+width**|**i+width+1**|




## 0.3 gst-itadf3left

this distance filter fills the pixel with 0 distance, by using the value of the pexel that just in the left of this pixel.

||||
|:-:|:-:|:-:|
|**i-1**|**i**||
||||


## 0.4 gst-itadf4leftmax

this distance filter fills the pixel with 0 distance, by using the **max** value of the pexels that in the top and bot and left of this pixel.

||||
|:-:|:-:|:-:|
|i-width-1|i-width||
|**i-1**|**i**||
|**i+width-1**|**i+width**||

## 0.5 gst-itadf5leftmin

this distance filter fills the pixel with 0 distance, by using the **max** value of the pexels that in the top and bot and left of this pixel.

||||
|:-:|:-:|:-:|
|i-width-1|i-width||
|**i-1**|**i**||
|**i+width-1**|**i+width**||


TODO

1. 4, 8,neighboor filters, left/left max/leftmin with widly neighboors.

5*5-1 = 24 Neighboors

||||||
|:-:|:-:|:-:|:-:|:-:|
|i-2\*width-2|i-2\*width-1|i-2\*width|i-2\*width+1|i-2\*width+2|
|**i-width-2**|**i-width-1**|**i-width**|**i-width+1**|**i-width+2**|
|**i-2**|**i-1**|**i**|**i+1**|**i+2**|
|**i+width-2**|**i+width-1**|**i+width**|**i+width+1**|**i+width+2**|
|**i+2\*width-2**|**i+2\*width-1**|**i+2\*width**|**i+2\*width+1**|**i+2\*width+2**|

6*6-1 = 35 neighboors

||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**i-3\*width-3**|**i-3\*width-2**|**i-3\*width-1**|**i-3\*width|i-3\*width+1**|**i-3\*width+2**|**i-3\*width+3**|
|**i-2\*width-3**|**i-2\*width-2**|**i-2\*width-1**|**i-2\*width**|**i-2\*width+1**|**i-2\*width+2**|**i-2\*width+3**|
|**i-width-3**|**i-width-2**|**i-width-1**|**i-width**|**i-width+1**|**i-width+2**|**i-width+3**|
|**i-3**|**i-2**|**i-1**|**i**|**i+1**|**i+2**|**i+3**|
|**i+width-3**|**i+width-2**|**i+width-1**|**i+width**|**i+width+1**|**i+width+2**|**i+width+3**|
|**i+2\*width-3**|**i+2\*width-2**|**i+2\*width-1**|**i+2\*width**|**i+2\*width+1**|**i+2\*width+2**|**i+2\*width+3**|
|**i+3\*width-3**|**i+3\*width-2**|**i+3\*width-1**|**i+3\*width**|**i+3\*width+1**|**i+3\*width+2**|**i+3\*width+3**|


2. filter to selece the distances
3. filter by using different frames


# 1. Folder Description

## 1.1 Tools and Template

### 1.1.1 gst-app-maker 

i haven't used it yet. 

### 1.1.2 gst -element-maker 

this tool is used to create a plugin with a certain template. the templates are saved in the folder `element-template`

the useage of this tool should follows the this syntax:

```
./gst -element-maker  plugin-name template
```

for example:

```
./gst -element-maker  filter videofilter
```

**note:** 

1. the template name can't be empty

2. this tool would create 2 files `.c` and `.h` in the **current directory**. So I suggest, we should firstly use `get-project-maker` to create a project with a good directory structure and create plugin **in the folder, that save the source files**.

3. we should note, that the **file name** of plugin would be automatically added `gst` as prefix without other characters. For example, plugin "filename" would be changed as "gstfimename". And the **hyphen** or **underscore** would be delected. So plugin with the filename *"file-name"* and *"file_name"* would have same filename *"gstfilename"*. **But** the name of plugin, when using the tool `gst-inspect-1.0` would still be the argument filename, that the user inputed in the command line, without the prefix "gst".

### 1.1.3 gst-project-maker

this tool creat a project with a certain project name. This tool would create a project with a good file structure. I suggest, that we could in the folder **plugin** in the folder system, that this tool created, use the `gst-element-maker`. 

### 1.1.4 Others 


### 1.1.5

### 1.1.6

### 1.1.7

### 1.1.8
