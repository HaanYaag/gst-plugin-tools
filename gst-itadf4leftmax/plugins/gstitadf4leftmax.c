/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstitadf4leftmax
 *
 * The itadf4leftmax element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! itadf4leftmax ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstitadf4leftmax.h"

GST_DEBUG_CATEGORY_STATIC (gst_itadf4leftmax_debug_category);
#define GST_CAT_DEFAULT gst_itadf4leftmax_debug_category

/* prototypes */


static void gst_itadf4leftmax_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_itadf4leftmax_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_itadf4leftmax_dispose (GObject * object);
static void gst_itadf4leftmax_finalize (GObject * object);

static gboolean gst_itadf4leftmax_start (GstBaseTransform * trans);
static gboolean gst_itadf4leftmax_stop (GstBaseTransform * trans);
static gboolean gst_itadf4leftmax_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_itadf4leftmax_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_itadf4leftmax_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_itadf4leftmax_src_template =
    GST_STATIC_PAD_TEMPLATE("src", GST_PAD_SRC, GST_PAD_ALWAYS,
                            GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{  GRAY16_LE  }")));

static GstStaticPadTemplate gst_itadf4leftmax_sink_template =
    GST_STATIC_PAD_TEMPLATE(
        "sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{ GRAY16_LE }")));

/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstItadf4leftmax, gst_itadf4leftmax, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_itadf4leftmax_debug_category, "itadf4leftmax", 0,
  "debug category for itadf4leftmax element"));

static void
gst_itadf4leftmax_class_init (GstItadf4leftmaxClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  // gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
  //     gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS,
  //       gst_caps_from_string (VIDEO_SRC_CAPS)));
  // gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
  //     gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
  //       gst_caps_from_string (VIDEO_SINK_CAPS)));

    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);


  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf4leftmax_sink_template);
  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf4leftmax_src_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "ita distance filter. max pixel in left 5 pixels", "Generic", "ita distance filter. max pixel in left 5 pixels",
      "Han <yhde93@gmail.com>");

  gobject_class->set_property = gst_itadf4leftmax_set_property;
  gobject_class->get_property = gst_itadf4leftmax_get_property;
  gobject_class->dispose = gst_itadf4leftmax_dispose;
  gobject_class->finalize = gst_itadf4leftmax_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_itadf4leftmax_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_itadf4leftmax_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_itadf4leftmax_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_itadf4leftmax_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_itadf4leftmax_transform_frame_ip);

}

static void
gst_itadf4leftmax_init (GstItadf4leftmax *itadf4leftmax)
{
}

void
gst_itadf4leftmax_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (object);

  GST_DEBUG_OBJECT (itadf4leftmax, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf4leftmax_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (object);

  GST_DEBUG_OBJECT (itadf4leftmax, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf4leftmax_dispose (GObject * object)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (object);

  GST_DEBUG_OBJECT (itadf4leftmax, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_itadf4leftmax_parent_class)->dispose (object);
}

void
gst_itadf4leftmax_finalize (GObject * object)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (object);

  GST_DEBUG_OBJECT (itadf4leftmax, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_itadf4leftmax_parent_class)->finalize (object);
}

static gboolean
gst_itadf4leftmax_start (GstBaseTransform * trans)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (trans);

  GST_DEBUG_OBJECT (itadf4leftmax, "start");

  return TRUE;
}

static gboolean
gst_itadf4leftmax_stop (GstBaseTransform * trans)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (trans);

  GST_DEBUG_OBJECT (itadf4leftmax, "stop");

  return TRUE;
}

static gboolean
gst_itadf4leftmax_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (filter);

  GST_DEBUG_OBJECT (itadf4leftmax, "set_info");

  return TRUE;
}

/* transform */
/**
 * this is the core of this filter
 * the empty pixel would be filtered by the max value in the 5 left neighbor pixels
 * besides the very left column and the very top raw, other pixels are possiable
*/
static GstFlowReturn
gst_itadf4leftmax_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (filter);
  gint width = GST_VIDEO_FRAME_WIDTH(inframe);
  gint height = GST_VIDEO_FRAME_HEIGHT(inframe);

  guint16 *in = (guint16 *)inframe->data[0];
  guint16 *out = (guint16 *)outframe->data[0];

 for (guint i = width+1; i < height*width-1; i++) {//from the second pixel in the 2. raw
      if(i % width != 0){
        if(in[i] == 0x0000){
          if(in[i - 1] > in[i + width] && in[i - 1] > in[i + width - 1] && in[i - 1] > in[i - width] && in[i - 1] > in[i - width - 1] ){
            out[i] = in[i - 1]; 
          } else 
          if(in[i + width] > in[i - 1] && in[i + width] > in[i + width - 1] && in[i + width] > in[i - width] && in[i + width] > in[i - width - 1] ){
            out[i] = in[i + width]; 
          } else
          if(in[i + width - 1] > in[i - 1] && in[i + width - 1] > in[i + width] && in[i + width - 1] > in[i - width] && in[i + width - 1] > in[i - width - 1] ){
            out[i] = in[i + width - 1]; 
          } else
          if(in[i - width] > in[i - 1] && in[i - width] > in[i + width] && in[i - width] > in[i + width - 1] && in[i - width] > in[i - width - 1] ){
            out[i] = in[i - width] ; 
          } else
          if(in[i - width - 1] > in[i - 1] && in[i - width - 1] > in[i + width] && in[i - width - 1] > in[i + width - 1] && in[i - width - 1] > in[i - width] ){
            out[i] = in[i - width - 1] ; 
          }
        }else{
          out[i] = in[i];
        }
        }
    }


  GST_DEBUG_OBJECT (itadf4leftmax, "transform_frame");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_itadf4leftmax_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstItadf4leftmax *itadf4leftmax = GST_ITADF4LEFTMAX (filter);

  GST_DEBUG_OBJECT (itadf4leftmax, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "itadf4leftmax", GST_RANK_NONE,
      GST_TYPE_ITADF4LEFTMAX);
}

/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://www.uni-hannover.de/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    itadf4leftmax,
    "ita distance filter, select the max pixel in the 5 left neighbor pixels to fill the blank pixel",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

