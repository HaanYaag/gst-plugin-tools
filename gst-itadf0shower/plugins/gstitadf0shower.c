/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstitadf0shower
 *
 * The itadf0shower element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! itadf0shower ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstitadf0shower.h"

GST_DEBUG_CATEGORY_STATIC (gst_itadf0shower_debug_category);
#define GST_CAT_DEFAULT gst_itadf0shower_debug_category

/* prototypes */


static void gst_itadf0shower_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_itadf0shower_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_itadf0shower_dispose (GObject * object);
static void gst_itadf0shower_finalize (GObject * object);

static gboolean gst_itadf0shower_start (GstBaseTransform * trans);
static gboolean gst_itadf0shower_stop (GstBaseTransform * trans);
static gboolean gst_itadf0shower_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_itadf0shower_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_itadf0shower_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_itadf0shower_src_template =
    GST_STATIC_PAD_TEMPLATE("src", GST_PAD_SRC, GST_PAD_ALWAYS,
                            GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{  GRAY16_LE  }")));

static GstStaticPadTemplate gst_itadf0shower_sink_template =
    GST_STATIC_PAD_TEMPLATE(
        "sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{ GRAY16_LE }")));

/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstItadf0shower, gst_itadf0shower, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_itadf0shower_debug_category, "itadf0shower", 0,
  "debug category for itadf0shower element"));

static void
gst_itadf0shower_class_init (GstItadf0showerClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

    GstElementClass *element_class = GST_ELEMENT_CLASS(klass);


  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf0shower_sink_template);
  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf0shower_src_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "ITA show 0 pixel", "Generic", "ITA show 0 pixel",
      "Han<yhde93@gmail.com>");

  gobject_class->set_property = gst_itadf0shower_set_property;
  gobject_class->get_property = gst_itadf0shower_get_property;
  gobject_class->dispose = gst_itadf0shower_dispose;
  gobject_class->finalize = gst_itadf0shower_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_itadf0shower_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_itadf0shower_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_itadf0shower_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_itadf0shower_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_itadf0shower_transform_frame_ip);

}

static void
gst_itadf0shower_init (GstItadf0shower *itadf0shower)
{
}

void
gst_itadf0shower_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (object);

  GST_DEBUG_OBJECT (itadf0shower, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf0shower_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (object);

  GST_DEBUG_OBJECT (itadf0shower, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf0shower_dispose (GObject * object)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (object);

  GST_DEBUG_OBJECT (itadf0shower, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_itadf0shower_parent_class)->dispose (object);
}

void
gst_itadf0shower_finalize (GObject * object)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (object);

  GST_DEBUG_OBJECT (itadf0shower, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_itadf0shower_parent_class)->finalize (object);
}

static gboolean
gst_itadf0shower_start (GstBaseTransform * trans)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (trans);

  GST_DEBUG_OBJECT (itadf0shower, "start");

  return TRUE;
}

static gboolean
gst_itadf0shower_stop (GstBaseTransform * trans)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (trans);

  GST_DEBUG_OBJECT (itadf0shower, "stop");

  return TRUE;
}

static gboolean
gst_itadf0shower_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (filter);

  GST_DEBUG_OBJECT (itadf0shower, "set_info");

  return TRUE;
}

/* transform */
/**
 * this is the core
*/
static GstFlowReturn
gst_itadf0shower_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (filter);
  
  gint width = GST_VIDEO_FRAME_WIDTH(inframe);
  gint height = GST_VIDEO_FRAME_HEIGHT(inframe);

  guint16 *in = (guint16 *)inframe->data[0];
  guint16 *out = (guint16 *)outframe->data[0];

int a = 0;
// g_print ("width: %d\n ",width);
for (guint i = 0; i < height*width; i++) {
  out[i] = in[i];
  if(i % width == 0){ // the first column
    g_print ("\n");
  }
  if(in[i] == 0){
    g_print ("o");
  } else {
    g_print (" ");
  }
}

g_print ("\n---------Another frame---------\n ");

  GST_DEBUG_OBJECT (itadf0shower, "transform_frame");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_itadf0shower_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstItadf0shower *itadf0shower = GST_ITADF0SHOWER (filter);

  GST_DEBUG_OBJECT (itadf0shower, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "itadf0shower", GST_RANK_NONE,
      GST_TYPE_ITADF0SHOWER);
}

/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://www.uni-hannover.de/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    itadf0shower,
    "ITA show 0 pixel",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

