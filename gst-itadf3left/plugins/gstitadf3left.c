/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstitadf3left
 *
 * The itadf3left element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! itadf3left ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstitadf3left.h"

GST_DEBUG_CATEGORY_STATIC (gst_itadf3left_debug_category);
#define GST_CAT_DEFAULT gst_itadf3left_debug_category

/* prototypes */


static void gst_itadf3left_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_itadf3left_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_itadf3left_dispose (GObject * object);
static void gst_itadf3left_finalize (GObject * object);

static gboolean gst_itadf3left_start (GstBaseTransform * trans);
static gboolean gst_itadf3left_stop (GstBaseTransform * trans);
static gboolean gst_itadf3left_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_itadf3left_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_itadf3left_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_itadf3left_src_template =
    GST_STATIC_PAD_TEMPLATE("src", GST_PAD_SRC, GST_PAD_ALWAYS,
                            GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{  GRAY16_LE  }")));

static GstStaticPadTemplate gst_itadf3left_sink_template =
    GST_STATIC_PAD_TEMPLATE(
        "sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{ GRAY16_LE }")));


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstItadf3left, gst_itadf3left, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_itadf3left_debug_category, "itadf3left", 0,
  "debug category for itadf3left element"));

static void
gst_itadf3left_class_init (GstItadf3leftClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  // gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
  //     gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS,
  //       gst_caps_from_string (VIDEO_SRC_CAPS)));
  // gst_element_class_add_pad_template (GST_ELEMENT_CLASS(klass),
  //     gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
  //       gst_caps_from_string (VIDEO_SINK_CAPS)));

  GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf3left_sink_template);
  gst_element_class_add_static_pad_template(element_class,
                                            &gst_itadf3left_src_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "ita distance filter with left pixel", "Generic", "ita distance filter with left pixel",
      "Han <yhde93@gmail.com>");

  gobject_class->set_property = gst_itadf3left_set_property;
  gobject_class->get_property = gst_itadf3left_get_property;
  gobject_class->dispose = gst_itadf3left_dispose;
  gobject_class->finalize = gst_itadf3left_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_itadf3left_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_itadf3left_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_itadf3left_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_itadf3left_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_itadf3left_transform_frame_ip);

}

static void
gst_itadf3left_init (GstItadf3left *itadf3left)
{
}

void
gst_itadf3left_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (object);

  GST_DEBUG_OBJECT (itadf3left, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf3left_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (object);

  GST_DEBUG_OBJECT (itadf3left, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_itadf3left_dispose (GObject * object)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (object);

  GST_DEBUG_OBJECT (itadf3left, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_itadf3left_parent_class)->dispose (object);
}

void
gst_itadf3left_finalize (GObject * object)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (object);

  GST_DEBUG_OBJECT (itadf3left, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_itadf3left_parent_class)->finalize (object);
}

static gboolean
gst_itadf3left_start (GstBaseTransform * trans)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (trans);

  GST_DEBUG_OBJECT (itadf3left, "start");

  return TRUE;
}

static gboolean
gst_itadf3left_stop (GstBaseTransform * trans)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (trans);

  GST_DEBUG_OBJECT (itadf3left, "stop");

  return TRUE;
}

static gboolean
gst_itadf3left_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (filter);

  GST_DEBUG_OBJECT (itadf3left, "set_info");

  return TRUE;
}

/* transform */
/**
 * this is the core of the filter
 * the pixel with 0 distance would directly use the left pixel
 * besides the pixels in the left colum, are all possiable
*/
static GstFlowReturn
gst_itadf3left_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (filter);
  gint width = GST_VIDEO_FRAME_WIDTH(inframe);
  gint height = GST_VIDEO_FRAME_HEIGHT(inframe);

  guint16 *in = (guint16 *)inframe->data[0];
  guint16 *out = (guint16 *)outframe->data[0];

  for (guint i = 1; i < height*width; i++) {
    if(i % width != 0){ // not the very left colum
        if(in[i] == 0x0000){
          out[i] = in[i - 1]; // the left
        } else{
          out[i] = in[i];
        }
    }
  }


  GST_DEBUG_OBJECT (itadf3left, "transform_frame");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_itadf3left_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstItadf3left *itadf3left = GST_ITADF3LEFT (filter);

  GST_DEBUG_OBJECT (itadf3left, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "itadf3left", GST_RANK_NONE,
      GST_TYPE_ITADF3LEFT);
}

/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://www.uni-hannover.de/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    itadf3left,
    "distance filter, using the left pixel. Besindes the left column, can be used for all other pixels",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

