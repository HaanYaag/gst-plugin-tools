/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_ITADF3LEFT_H_
#define _GST_ITADF3LEFT_H_

#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>

G_BEGIN_DECLS

#define GST_TYPE_ITADF3LEFT   (gst_itadf3left_get_type())
#define GST_ITADF3LEFT(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ITADF3LEFT,GstItadf3left))
#define GST_ITADF3LEFT_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ITADF3LEFT,GstItadf3leftClass))
#define GST_IS_ITADF3LEFT(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ITADF3LEFT))
#define GST_IS_ITADF3LEFT_CLASS(obj)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ITADF3LEFT))

typedef struct _GstItadf3left GstItadf3left;
typedef struct _GstItadf3leftClass GstItadf3leftClass;

struct _GstItadf3left
{
  GstVideoFilter base_itadf3left;

};

struct _GstItadf3leftClass
{
  GstVideoFilterClass base_itadf3left_class;
};

GType gst_itadf3left_get_type (void);

G_END_DECLS

#endif
