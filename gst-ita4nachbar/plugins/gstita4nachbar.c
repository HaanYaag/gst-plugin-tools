/* GStreamer
 * Copyright (C) 2020 FIXME <fixme@example.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstita4nachbar
 *
 * The ita4nachbar element does FIXME stuff.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v fakesrc ! ita4nachbar ! FIXME ! fakesink
 * ]|
 * FIXME Describe what the pipeline does.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include "gstita4nachbar.h"

GST_DEBUG_CATEGORY_STATIC (gst_ita4nachbar_debug_category);
#define GST_CAT_DEFAULT gst_ita4nachbar_debug_category

/* prototypes */


static void gst_ita4nachbar_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_ita4nachbar_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_ita4nachbar_dispose (GObject * object);
static void gst_ita4nachbar_finalize (GObject * object);

static gboolean gst_ita4nachbar_start (GstBaseTransform * trans);
static gboolean gst_ita4nachbar_stop (GstBaseTransform * trans);
static gboolean gst_ita4nachbar_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info);
static GstFlowReturn gst_ita4nachbar_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe);
static GstFlowReturn gst_ita4nachbar_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);

enum
{
  PROP_0
};

/* pad templates */
static GstStaticPadTemplate gst_ita4nachbar_src_template =
    GST_STATIC_PAD_TEMPLATE("src", GST_PAD_SRC, GST_PAD_ALWAYS,
                            GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{  GRAY16_LE  }")));

static GstStaticPadTemplate gst_ita4nachbar_sink_template =
    GST_STATIC_PAD_TEMPLATE(
        "sink", GST_PAD_SINK, GST_PAD_ALWAYS,
        GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("{ GRAY16_LE }")));



/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstIta4nachbar, gst_ita4nachbar, GST_TYPE_VIDEO_FILTER,
  GST_DEBUG_CATEGORY_INIT (gst_ita4nachbar_debug_category, "ita4nachbar", 0,
  "debug category for ita4nachbar element"));

static void
gst_ita4nachbar_class_init (GstIta4nachbarClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *video_filter_class = GST_VIDEO_FILTER_CLASS (klass);
 GstElementClass *element_class = GST_ELEMENT_CLASS(klass);

  /* Setting up pads and setting metadata should be moved to
     base_class_init if you intend to subclass this class. */
  gst_element_class_add_static_pad_template (element_class,
      &gst_ita4nachbar_sink_template);
  gst_element_class_add_static_pad_template (element_class,
      &gst_ita4nachbar_src_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
      "ITA_Distance_Filter_4_Nachbar", "ITA_Distance_Filter_4_Nachbar", "ITA_Distance_Filter_4_Nachbar",
      "Han <yhde93@gmail.com>");

  gobject_class->set_property = gst_ita4nachbar_set_property;
  gobject_class->get_property = gst_ita4nachbar_get_property;
  gobject_class->dispose = gst_ita4nachbar_dispose;
  gobject_class->finalize = gst_ita4nachbar_finalize;
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_ita4nachbar_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_ita4nachbar_stop);
  video_filter_class->set_info = GST_DEBUG_FUNCPTR (gst_ita4nachbar_set_info);
  video_filter_class->transform_frame = GST_DEBUG_FUNCPTR (gst_ita4nachbar_transform_frame);
  video_filter_class->transform_frame_ip = GST_DEBUG_FUNCPTR (gst_ita4nachbar_transform_frame_ip);

}

static void
gst_ita4nachbar_init (GstIta4nachbar *ita4nachbar)
{
}

void
gst_ita4nachbar_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (object);

  GST_DEBUG_OBJECT (ita4nachbar, "set_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_ita4nachbar_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (object);

  GST_DEBUG_OBJECT (ita4nachbar, "get_property");

  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_ita4nachbar_dispose (GObject * object)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (object);

  GST_DEBUG_OBJECT (ita4nachbar, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_ita4nachbar_parent_class)->dispose (object);
}

void
gst_ita4nachbar_finalize (GObject * object)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (object);

  GST_DEBUG_OBJECT (ita4nachbar, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_ita4nachbar_parent_class)->finalize (object);
}

static gboolean
gst_ita4nachbar_start (GstBaseTransform * trans)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (trans);

  GST_DEBUG_OBJECT (ita4nachbar, "start");

  return TRUE;
}


static gboolean
gst_ita4nachbar_stop (GstBaseTransform * trans)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (trans);

  GST_DEBUG_OBJECT (ita4nachbar, "stop");

  return TRUE;
}

static gboolean
gst_ita4nachbar_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (filter);

  GST_DEBUG_OBJECT (ita4nachbar, "set_info");

  return TRUE;
}

/* transform */
/**
 * the core funtion of this filter
 * 
 * 
 * 
*/
static GstFlowReturn
gst_ita4nachbar_transform_frame (GstVideoFilter * filter, GstVideoFrame * inframe,
    GstVideoFrame * outframe)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (filter);
  gint width = GST_VIDEO_FRAME_WIDTH(inframe);
  gint height = GST_VIDEO_FRAME_HEIGHT(inframe);
 //g_print ("[running ita-4-nachbar distance filter]\n ");

  guint16 *in = (guint16 *)inframe->data[0];
  guint16 *out = (guint16 *)outframe->data[0];


    for (guint i = width+1; i < (height-1)*width-1; i++) {
      if(i % width != 0 || (i + 1)%width != 0){
        if(in[i] == 0x0000){
          /* 4 neighborhood */
          out[i] = (in[i + 1] + in[i - 1] + in[i + width] + in[i - width])  / 4;  //4 neighborhood
        } else{
          out[i] = in[i];
        }
              }
  }



  GST_DEBUG_OBJECT (ita4nachbar, "transform_frame");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_ita4nachbar_transform_frame_ip (GstVideoFilter * filter, GstVideoFrame * frame)
{
  GstIta4nachbar *ita4nachbar = GST_ITA4NACHBAR (filter);

  GST_DEBUG_OBJECT (ita4nachbar, "transform_frame_ip");

  return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{

  /* FIXME Remember to set the rank if it's an element that is meant
     to be autoplugged by decodebin. */
  return gst_element_register (plugin, "ita4nachbar", GST_RANK_NONE,
      GST_TYPE_ITA4NACHBAR);
}

/* FIXME: these are normally defined by the GStreamer build system.
   If you are creating an element to be included in gst-plugins-*,
   remove these, as they're always defined.  Otherwise, edit as
   appropriate for your external plugin package. */
#ifndef VERSION
#define VERSION "0.0.FIXME"
#endif
#ifndef PACKAGE
#define PACKAGE "FIXME_package"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "FIXME_package_name"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://FIXME.org/"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    ita4nachbar,
    "FIXME plugin description",
    plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

