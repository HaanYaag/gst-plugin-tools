'''
this is a program that shows the result of filters
the filters are used in in distance image in Gstreamer,
it would fill the pixel, that has no distance value, and shown as black in RGB image.

in this program, the input image is not distance image, it is a RGB image
the RGB image could also also the distribution of distance.
but there are some things, that differ from distance filter.
1. because of the data type of the RGB image, the color were compressed.
    so that, the black images are not pure black. for example RGB(1,1,1) looks like black, but not (0,0,0)
2. the distance "Image" has only one "color". bigger value means far, small value mean near.
    but in RGB image, there are 3 channels (R,G,B). the "max or min value" are different from "distance"
so in this program there are two things are noticable:
1. I convert the RGB image into an image with pure black. (1,1,1)->(0,0,0).
2. I select the max and min value by counting the sum of R,G,B value. max = max(R,G,B)
these makes this plan more similar as the real filter in Gstreamer.

and the effect in Gstreamer may not so "good" like in this program,
1. because in the Gstreamer, the data flows as Streaming.
it may not able to filter all pixels in each Frame with a complex algorithm
because there are more than 1000*1000 pixels, but a frame is only 1 s lang.
2. because of streaming, it is also likely to lost some data.

NOTE:
    1. in each function of filter, there are a code to save the image in computer.
    2. the functions that could be modified are noted with [!]
Version: 1.0
Date: 11.11.2020
Author: Han Yang
Tutor: Lukas Juette

'''

import numpy as np
from cv2 import cv2 #import opencv-python or pip install opencv-python

Address_of_image = "image.jpeg" # TEST
image = cv2.imread(Address_of_image)


'''
this part is to make the .jpeg image in to an image with pure-black pixel
because the image is not an real distance image, and also because of .jpeg
-> the pixel with black color are not pure black. 
'''
height = image.shape[0]  #(height，width，channels)
width = image.shape[1]
channels = image.shape[2]
black_pixel = 0
img_creat = np.zeros([height, width, channels], dtype=np.uint8) # create an new image
img_creat[:,:,:] = np.all([height,width])*255   # set image white
threshold = 110
for row in range(height):
    for col in range(width):
        # if(np.all(image[row,col] == 0)):
        if (image.item(row, col,0) < threshold and image.item(row, col,1) < threshold and image.item(row, col,2) < threshold ):
            # because the image is not a distance image, and it was also compressed because of the data type: jepg.
            # some area with black pixels (which means with 0 distance), are not shown as "black" RGB(0,0,0),
            # but are actually shown as a color similar as black, but the RGB value is not 0.
            # so in this step:
            # we are going to extract the pixels with the color similar as black.
            # i set a threshold 100.
            # and then convert it into a binary image
            black_pixel=black_pixel+1
            # img_creat.itemset((row, col, 0),image.item(row, col, 0))
            # img_creat.itemset((row, col, 1), image.item(row, col, 1))
            # img_creat.itemset((row, col, 2), image.item(row, col, 2))
            # img_creat[row, col, :] = np.all([height, width]) * 0
            img_creat[row, col, :] = np.all([height, width]) * 0
        else:
            img_creat.itemset((row, col, 0),image.item(row, col, 0))
            img_creat.itemset((row, col, 1), image.item(row, col, 1))
            img_creat.itemset((row, col, 2), image.item(row, col, 2))
            # img_creat[row, col, :] = np.all([height, width]) * 0    # set black pixel on same position on new image
            # print(image[row,col])
print("area of image:", height * width)
print("pixel / area:",black_pixel/(height * width))
print("* black pixel in all image: ", black_pixel)
# [!][this part can show the original image]
# cv2.imshow("image",image)
# cv2.waitKey(0)

# [!][this part show the image, that be pured.]
cv2.imshow("created_img",img_creat)
cv2.waitKey(0)

'''
# [this part could save the pured image in computer]
# cv2.imwrite('image_pure_blk.png', img_creat)

# then, we will try to use the filter in the new-created image.
# we should before using a filter, creat an new image.

# this function is a filter to fill each black (or white) pixel by using the average value of 4 nachbar pixel.
# to simulate the procedure in Gstramer, we would scan each row, from left to right
# Syntax: image[row,col] is type 'numpy.ndarray'
'''

# [!] 0,0,0 is black
color = [0,0,0]


def filter_4_nachbar(image):
    height = image.shape[0]  # (height，width，channels)
    width = image.shape[1]
    channels = image.shape[2]
    for row in range(height):
        for col in range(width):
            # if(image.item(row, col,0) == 0 and image.item(row, col,1) == 0 and image.item(row, col,2) == 0):
            # np.all: AND / np.anyL: OR. image[row,col] == [0,0,0] would return 3 logical results.
            if(np.all(image[row,col] == color)):
                # print(row,col)
                # print(type(image.item(row + 1, col, 0)))
                # note: here is a problem: the distance image is an image with only a value.
                # but this is an RGB-image, it contains 3 values, it contains R-G-B values.
                # perhaps i would calculate the average value of both R,G,B value
                if(row > 0 and row < height-1 and col < width-1 and col > 0):
                    R = image.item(row+1,col,0)+image.item(row-1,col,0)+image.item(row,col+1,0)+image.item(row,col-1,0)
                    G = image.item(row+1, col, 1) + image.item(row-1, col, 1) + image.item(row, col+1, 1) + image.item(row,
                                                                                                              col-1, 1)
                    B = image.item(row+1, col, 2) + image.item(row-1, col, 2) + image.item(row, col+1, 2) + image.item(row,
                                                                                                            col-1, 2)
                    image.itemset((row,col,0),B/4)
                    image.itemset((row, col, 1),G/4)
                    image.itemset((row, col, 2),R/4)
    # [!][this function would save and show the result]
    cv2.imwrite('image_after_4.png', image)
    cv2.imshow("img_4_nachbar",image)
    cv2.waitKey(0)
# [!][this part is to use the function: 4 nachbar filter]
# image_4_nachbar = img_creat.copy()
# filter_4_nachbar(image_4_nachbar)

'''
this function use average of 8 neighbor pixels to fill the black pixel
'''

def filter_8_nachbar(image):
    height = image.shape[0]  # (height，width，channels)
    width = image.shape[1]
    channels = image.shape[2]
    for row in range(height):
        for col in range(width):
            if(np.all(image[row,col] == color)):
                if (row > 0 and row < height - 1 and col < width - 1 and col > 0):
                    # \ means change line
                    R = image.item(row+1,col,0)+image.item(row-1,col,0)\
                        +image.item(row,col+1,0)+image.item(row,col-1,0)\
                        +image.item(row-1, col-1, 0)+image.item(row+1,col+1,0)\
                        +image.item(row-1, col+1, 0)+image.item(row+1,col-1,0)
                    G = image.item(row+1, col, 1) + image.item(row-1, col, 1)\
                        + image.item(row, col+1, 1) + image.item(row,col-1, 1)\
                        +image.item(row - 1, col - 1, 1) + image.item(row + 1, col + 1, 1)\
                        +image.item(row - 1, col + 1, 1) + image.item(row + 1, col - 1, 1)
                    B = image.item(row+1, col, 2) + image.item(row-1, col, 2)\
                        + image.item(row, col+1, 2) + image.item(row,col-1, 2)\
                        +image.item(row - 1, col - 1, 2) + image.item(row + 1, col + 1, 2)\
                        +image.item(row - 1, col + 1, 2) + image.item(row + 1, col - 1, 2)
                    image.itemset((row,col,0),B/8)
                    image.itemset((row, col, 1),G/8)
                    image.itemset((row, col, 2),R/8)
    # [!][this function would save and show the result]
    cv2.imwrite('image_after_8.png', image) # if you don't want to save in file, make it in comment
    cv2.imshow("img_8_nachbar", image)
    cv2.waitKey(0)
# [!][this part is to use the function: 8 nachbar filter]
# image_8_nachbar = img_creat.copy()
# filter_8_nachbar(image_8_nachbar)


'''
this function is the filter that use the left pixel to fill the black pixel 
'''
def filter_left(image):
    height = image.shape[0]  # (height，width，channels)
    width = image.shape[1]
    channels = image.shape[2]
    for row in range(height):
        for col in range(width):
            if (np.all(image[row, col] == color)):
                if (col > 0):
                    image.itemset((row, col, 0), image.item(row,col-1,0))
                    image.itemset((row, col, 1), image.item(row,col-1,1))
                    image.itemset((row, col, 2), image.item(row,col-1,2))
    # [!][this function would save and show the result]
    cv2.imwrite('image_after_left.png', image)
    cv2.imshow("image_after_left", image)
    cv2.waitKey(0)
# [!][this part is to use the function: left filter]
# image_left = img_creat.copy()
# filter_left(image_left)

'''
this function is the filter that use the pixel with max (RGB)value to fill the black pixel 
'''
def filter_left_max(image):
    height = image.shape[0]  # (height，width，channels)
    width = image.shape[1]
    channels = image.shape[2]
    for row in range(height):
        for col in range(width):
            if (np.all(image[row, col] == color)):
                if (row > 0 and row < height - 1 and col > 0):
                    S = [image.item(row - 1, col, 0) + image.item(row - 1, col, 1) + image.item(row - 1, col, 2)
                        , image.item(row + 1, col, 0) + image.item(row + 1, col, 1) + image.item(row + 1, col, 2)
                        , image.item(row, col - 1, 0) + image.item(row, col - 1, 1) + image.item(row, col - 1, 2)
                        , image.item(row - 1, col - 1, 0) + image.item(row - 1, col - 1, 1) + image.item(row - 1, col - 1, 2)
                        , image.item(row + 1, col - 1, 0) + image.item(row + 1, col - 1, 1) + image.item(row + 1, col - 1, 2)
                         ]
                    R = [image.item(row - 1, col, 0)
                        , image.item(row + 1, col, 0)
                        , image.item(row, col - 1, 0)
                        , image.item(row - 1, col - 1, 0)
                        , image.item(row + 1, col - 1, 0)]
                    G = [image.item(row - 1, col, 1)
                        , image.item(row + 1, col, 1)
                        , image.item(row, col - 1, 1)
                        , image.item(row - 1, col - 1, 1)
                        , image.item(row + 1, col - 1, 1)]
                    B = [image.item(row - 1, col, 2)
                        , image.item(row + 1, col, 2)
                        , image.item(row, col - 1, 2)
                        , image.item(row - 1, col - 1, 2)
                        , image.item(row + 1, col - 1, 2)]
                    maxIndex = S.index(max(S))
                    image.itemset((row, col, 0), R[maxIndex])
                    image.itemset((row, col, 1), G[maxIndex])
                    image.itemset((row, col, 2), B[maxIndex])
    # [!][this function would save and show the result]
    cv2.imwrite('image_after_left_max.png', image)
    cv2.imshow("image_after_left_max", image)
    cv2.waitKey(0)
# [!][this part is to use the function: left max filter]
# image_left_max = img_creat.copy()
# filter_left_max(image_left_max)

'''
this function is the filter that use the pixel with min (RGB)value to fill the black pixel 
'''
def filter_left_min(image):
    height = image.shape[0]  # (height，width，channels)
    width = image.shape[1]
    channels = image.shape[2]

    for row in range(height):
        for col in range(width):
            if (np.all(image[row, col] == color)):
                # X = []
                # Y = []
                S = []
                R = []
                G = []
                B = []
                if (row > 0 and row < height - 1 and col > 0):
                    if(np.all(image[row - 1, col] != color)):
                        # X.append(row-1)
                        # Y.append(col)
                        R.append(image.item(row - 1, col,0))
                        G.append(image.item(row - 1, col,1))
                        B.append(image.item(row - 1, col,2))
                    if (np.all(image[row + 1, col] != color)):
                        # X.append(row+1)
                        # Y.append(col)
                        R.append(image.item(row + 1, col,0))
                        G.append(image.item(row + 1, col,1))
                        B.append(image.item(row + 1, col,2))
                    if (np.all(image[row, col - 1] != color)):
                        # X.append(row)
                        # Y.append(col-1)
                        R.append(image.item(row, col-1,0))
                        G.append(image.item(row, col-1,1))
                        B.append(image.item(row, col-1,2))
                    if (np.all(image[row - 1, col - 1] != color)):
                        # X.append(row-1)
                        # Y.append(col-1)
                        R.append(image.item(row - 1, col-1,0))
                        G.append(image.item(row - 1, col-1,1))
                        B.append(image.item(row - 1, col-1,2))
                    if (np.all(image[row + 1, col - 1] != color)):
                        # X.append(row+1)
                        # Y.append(col-1)
                        R.append(image.item(row + 1, col-1,0))
                        G.append(image.item(row + 1, col-1,1))
                        B.append(image.item(row + 1, col-1,2))
                    # print(len(R))
                    for i in range(len(R)):
                            S.append(R[i] + B[i] + G[i])
                    # print(S)
                    if (len(S) != 0):
                        minIndex = S.index(min(S))
                        image.itemset((row, col, 0), R[minIndex])
                        image.itemset((row, col, 1), G[minIndex])
                        image.itemset((row, col, 2), B[minIndex])
    # [!][this function would save and show the result]
    cv2.imwrite('image_after_left_min.png', image)
    cv2.imshow("image_after_left_min", image)
    cv2.waitKey(0)
# [!][this part is to use the function: left min filter]
# image_left_min = img_creat.copy()
# filter_left_min(image_left_min)
